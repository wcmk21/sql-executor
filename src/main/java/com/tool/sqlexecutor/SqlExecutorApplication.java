package com.tool.sqlexecutor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.integration.config.EnableIntegrationManagement;
import org.springframework.integration.config.IntegrationManagementConfiguration;
import org.springframework.util.StringUtils;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, IntegrationAutoConfiguration.class})
public class SqlExecutorApplication {

    public static void main(String[] args) {
        String[] disabledCommands = {"--spring.shell.command.script.enabled=false"};
        String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);
        SpringApplication.run(SqlExecutorApplication.class, fullArgs);
    }

}
