package com.tool.sqlexecutor.util;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import java.io.File;

public final class SqlResourceBuilder {
    public static Resource getResource(String sql) {
        return new ByteArrayResource(sql.getBytes());
    }

    public static Resource getResource(File script) {
        return new DefaultResourceLoader().getResource("file:" + script.getAbsolutePath());
    }
}
