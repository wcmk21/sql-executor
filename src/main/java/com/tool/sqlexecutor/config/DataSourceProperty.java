package com.tool.sqlexecutor.config;

public class DataSourceProperty {
    private static final int DEFAULT_PORT = 3306;
    private SSH ssh;
    private String host;
    private int port = DEFAULT_PORT;
    private String database;
    private String user;
    private String password;

    public SSH getSsh() {
        return ssh;
    }

    public void setSsh(SSH ssh) {
        this.ssh = ssh;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (this.port <= 0) {
            this.port = DEFAULT_PORT;
        } else {
            this.port = port;
        }
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDatabase() + "?useUnicode=true&characterEncoding=utf8";
    }
}
