package com.tool.sqlexecutor.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Config {
    Map<String, List<DataSourceProperty>> server = new HashMap<>();

    public Map<String, List<DataSourceProperty>> getServer() {
        return server;
    }

    public void setServer(Map<String, List<DataSourceProperty>> server) {
        this.server = server;
    }
}
