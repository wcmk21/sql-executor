package com.tool.sqlexecutor.config;

public enum ConfigCache {
    INSTANCE;
    private Config config;

    ConfigCache() {
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Config getConfig() {
        return this.config;
    }
}
