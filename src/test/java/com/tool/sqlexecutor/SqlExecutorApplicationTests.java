package com.tool.sqlexecutor;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.shell.table.BorderStyle;
import org.springframework.shell.table.TableBuilder;
import org.springframework.shell.table.TableModelBuilder;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.sql.DataSource;

class SqlExecutorApplicationTests {
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";

    @Test
    void contextLoads() throws JSchException {
        Session session = new JSch().getSession("root", "127.0.0.1", 22);
        session.setPassword("123456");
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();

        try {
            int port = session.setPortForwardingL(0, "mysql-dev0", 3306);

            DataSource dataSource = DataSourceBuilder.create()
                    .type(DriverManagerDataSource.class)
                    .driverClassName(JDBC_DRIVER)
                    .url("jdbc:mysql://127.0.0.1:" + port + "/test?useUnicode=true&characterEncoding=utf8")
                    .username("root")
                    .password("123456")
                    .build();

            DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
            transactionManager.setEnforceReadOnly(true);
            transactionManager.getTransaction(new DefaultTransactionDefinition());

            JdbcTemplate template = new JdbcTemplate(dataSource);
            SqlRowSet sqlRowSet = template.queryForRowSet("select 1 from dual;");
            System.out.println(Arrays.array(sqlRowSet.getMetaData().getColumnNames()).toString());
        } catch (Exception e) {
            throw e;
        } finally {
            session.disconnect();
        }


    }
}
