# sql-executor

#### 介绍

为了解决同一个脚本同一时间在多个数据库上执行，开发的小工具；

当前只支持MYSQL；

依赖spring-boot和spring-shell开发；

#### 基本用法

- 可以直接在命令行执行，并指定配置文件、数据库、以及脚本
    
    ```shell script
    java -jar sql-executor-0.0.1-SNAPSHOT.jar --config=config.json  --env=DEV --script=test.sql
    // 或 默认情况下加载当前目录下的config.json文件
    java -jar sql-executor-0.0.1-SNAPSHOT.jar --env=DEV --script=test.sql
    ```

- 也可以进入shell命令行

    ```shell script
    java -jar sql-executor-0.0.1-SNAPSHOT.jar
    ```
- 可以参看帮助

    ```shell script
    shell:>help
    AVAILABLE COMMANDS
    
    Built-In Commands
            clear: Clear the shell screen.
            exit, quit: Exit the shell.
            help: Display help about available commands.
            history: Display or save the history of previously run commands
            stacktrace: Display the full stacktrace of the last error.
    
    Config Commands
            config: 查看配置信息
            reset: 配置数据源,默认加载config.json
    
    Sql Commands
            query: 执行查询语句
            run: 执行指定脚本,可以指定脚本文件,也可以指定sql,如果指定sql则忽略脚本文件
    ```
- 主要提供了三个命令

    - reset [file] 重置配置文件
    - run -e DEV -f [file] 执行DML语句
    - query -e DEV -s "select 1 from dual;" 执行查询语句，只读
    
- 也可以查询单个命令的帮助信息

    ```shell script
    shell:>help run
    
    
    NAME
            run - 执行指定脚本,可以指定脚本文件,也可以指定sql,如果指定sql则忽略脚本文件
    
    SYNOPSYS
            run [--env] string  [[--index] int]  [[--sql] string]  [[--script] file]  [[--separator] string]
    
    OPTIONS
            --env or -e  string
                    指定执行环境,比如 DEV;也可以使用[;,/]等符号指定多个环境
                    [Mandatory]
    
            --index or -i  int
                    配合env使用,用来指定特定数据库实例
                    [Optional, default = -1]
    
            --sql or -s  string
                    待执行的SQL语句
                    [Optional, default = ]
    
            --script or --file or -f  file
                    待执行的脚本文件
                    [Optional, default = ]
    
            --separator  string
                    指定切分SQL的字符，默认;
                    [Optional, default = ]
    ```
  
    **注意，本质上是通过separator切分sql,然后单个SQL执行; 如果创建存储过程的话，指定--separator=;;,语句如下
    
    ```sql
      CREATE PROCEDURE delete_matches(IN p_playerno INTEGER)
      BEGIN
      DELETE FROM t_test
      WHERE id = p_playerno;
      END;;
    ```

#### 配置文件格式

```shell script
{
  "server": {
    "DEV": [
      {
        "ssh": {
          "enable": true,
          "host": "localhost",
          "port": 22,
          "user": "root",
          "password": "12345678"
        },
        "host": "mysql-dev0",
        "port": 3306,
        "database": "test",
        "user": "root",
        "password": "123456"
      }
    ]
  }
}
```

** 注意：ssh测试通过，不使用的话可以不配置